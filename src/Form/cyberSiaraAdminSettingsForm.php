<?php

namespace Drupal\siarashield\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure reCAPTCHA settings for this site.
 */
class cyberSiaraAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cybersiara_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['siarashield.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('siarashield.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('SiaraShield settings'),
      '#open' => TRUE,
    ];

    $form['general']['cybersiara_public_key'] = [
      '#default_value' => $config->get('public_key'),
      '#description' => $this->t('Get your public & private key from <a href=":url">mycybersiara.com</a>.', [':url' => 'https://mycybersiara.com/Register/']),
      '#maxlength' => 40,
      '#required' => TRUE,
      '#title' => $this->t('SiaraShield Public key'),
      '#type' => 'textfield',
    ];

    $form['general']['cybersiara_private_key'] = [
      '#default_value' => $config->get('private_key'),
      '#description' => $this->t('Get your public & private key from <a href=":url">mycybersiara.com</a>.', [':url' => 'https://mycybersiara.com/Register/']),
      '#maxlength' => 40,
      '#required' => TRUE,
      '#title' => $this->t('SiaraShield Private key'),
      '#type' => 'textfield',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('siarashield.settings');
    $config
      ->set('public_key', $form_state->getValue('cybersiara_public_key'))
      ->set('private_key', $form_state->getValue('cybersiara_private_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
